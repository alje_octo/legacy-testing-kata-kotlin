package com.octo.legacy.testing.quotebot

class AutomaticQuoteBot {
    fun sendAllQuotes(mode: String) {
        val blogs = AdSpace.getAdSpaces()

        for (blog in blogs) {
            val blogAuctionTask = BlogAuctionTask()
            blogAuctionTask.priceAndPublish(blog, mode)
        }
    }

}