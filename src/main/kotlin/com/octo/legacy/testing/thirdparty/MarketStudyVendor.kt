package com.octo.legacy.testing.thirdparty

import java.util.*
import javax.swing.JOptionPane

class MarketStudyVendor {
    fun averagePrice(blog: String): Double {
        if (System.getenv("license") == null) {
            JOptionPane.showMessageDialog(
                null, "Missing license !",
                "Stupid license", JOptionPane.WARNING_MESSAGE
            )
            throw RuntimeException("Missing license")
        }
        return blog.hashCode().toDouble() * Random().nextDouble()
    }

}